/*
 * ADC.h
 *
 * Created: 8. 05. 2016 14:04:08
 *  Author: Jan
 */ 


#ifndef ADC_H_
#define ADC_H_

#include <avr/io.h>

void Init_ADC();
unsigned int ADConvert(char ch);


#endif /* ADC_H_ */