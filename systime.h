/*
 * systime.h
 *
 * Created: 16. 05. 2016 21:40:08
 *  Author: Jan
 */ 


#ifndef SYSTIME_H_
#define SYSTIME_H_

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/atomic.h>

#define F_1ms 0

void Init_IO();
void Init_T2();
void Init_T0();
uint32_t GetSysTick();
char HasOneMillisecondPassed();

extern volatile uint32_t systick;
extern volatile uint8_t TIME_STATUS;

#endif /* SYSTIME_H_ */