/*
 * pwm.c
 *
 * Created: 2. 03. 2017 18:12:58
 * Author: Jan
 * PROBLEM V TEM DA ISTI TIMER UPORABLA IR IN PWM KO ZVO�NIK DELA IR ITK NE LOVI KER JE ALARM
 * NA�ELOMA NAJ TO POL NEBIBIL PROBLEM
 * SAM USEEM PIZDARIJA
 * prescaler 8 najbl te�n za poslu�at prporo�am
 */ 

#include "pwm.h"

void PWM_init(int p){ //kot pwm ocr1B se nastav pol
	//fast pwm, clear oc1b on compare match, no prescaler, TOP 0x3ff
	SPEAKER_DIR|=(1<<SPEAKER_PIN_VAL);
	TCCR1A=(1<<COM1B1)|(1<<WGM11)|(1<<WGM10);
	TCCR1B=PRE0|(1<<WGM12);
	prescaler=p;
	
}

void PWM_stop(){
	TCCR1B=PRE0|(1<<WGM12);
}

void PWM_start(){
	OCR1B=0x3ff/2;
	switch (prescaler){
		case 1:			TCCR1B=PRE1|(1<<WGM12); break;
		case 8:			TCCR1B=PRE8|(1<<WGM12); break;
		case 64:		TCCR1B=PRE64|(1<<WGM12); break;
		case 256:		TCCR1B=PRE256|(1<<WGM12); break;
		case 1024:		TCCR1B=PRE1024|(1<<WGM12); break;
		default:		TCCR1B=PRE8|(1<<WGM12); break;
	}
}