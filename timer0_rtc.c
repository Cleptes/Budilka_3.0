/*
 * timer0_rtc.c
 *
 * Created: 5. 03. 2017 11:04:10
 *  Author: Jan
 */ 

#include "timer0_rtc.h"

volatile uint16_t ms;
volatile struct tm *rtc_time;
volatile time_t s_since_Y2K;

void timer0_rtc_init(){
	//nastavi daylight saving time pa zone
	//set_zone(1*ONE_HOUR);
	
	TCCR0A=_BV(WGM01); //CTC mode
	OCR0A=249;	
	TIMSK0=_BV(OCIE0A); // vklop interupta ko ura dose�e ocr0a
	TCNT0=0;
	ms=0;
	s_since_Y2K=0;
	TCCR0B=_BV(CS01)|_BV(CS00); //prescaler 64 ura je 250000Hz -> 4e-6 -> �teje do 249 da je 1 ms
}

char month2char(char m){
	if(m<10) return m+'0';
	else if(m==10) return 'O';
	else if(m==11) return 'N';
	else if(m==12) return 'D';
	else return 'e';
}

void time2asc(struct tm* time_date,char *buff){
	int i=0;
	
	//dnevi 2 mesta
	if(time_date->tm_mday<10){
		*(buff+i)='0';
		i++;
		*(buff+i)='0'+time_date->tm_mday;
		i++;
	}else{
		*(buff+i)='0'+time_date->tm_mday/10;
		i++;
		*(buff+i)='0'+time_date->tm_mday%10;
		i++;
	}
	
	*(buff+i)=' ';
	i++;
	
	//mesec 1 mesto
	*(buff+i)=month2char(time_date->tm_mon);
	i++;
	
	*(buff+i)=' ';
	i++;
	
	//leto 2 mesti
	char tmp=time_date->tm_year%100;
	*(buff+i)='0'+tmp/10;
	i++;
	*(buff+i)='0'+tmp%10;
	i++;
	
	*(buff+i)=' ';
	i++;
	
	//ura 2 mesti
	if(time_date->tm_hour<10){
		*(buff+i)='0';
		i++;
		*(buff+i)='0'+time_date->tm_hour;
		i++;
		}else{
		*(buff+i)='0'+time_date->tm_hour/10;
		i++;
		*(buff+i)='0'+time_date->tm_hour%10;
		i++;
	}
	
	*(buff+i)=':';
	i++;
	
	//min 2 mesti
	if(time_date->tm_min<10){
		*(buff+i)='0';
		i++;
		*(buff+i)='0'+time_date->tm_min;
		i++;
		}else{
		*(buff+i)='0'+time_date->tm_min/10;
		i++;
		*(buff+i)='0'+time_date->tm_min%10;
		i++;
	}
	
	*(buff+i)=':';
	i++;
		
	//sec 2 mesti
	if(time_date->tm_sec<10){
		*(buff+i)='0';
		i++;
		*(buff+i)='0'+time_date->tm_sec;
		i++;
		}else{
		*(buff+i)='0'+time_date->tm_sec/10;
		i++;
		*(buff+i)='0'+time_date->tm_sec%10;
		i++;
	}
	
	*(buff+i)=0;
	i++;

}

ISR(TIMER0_COMPA_vect){
	if(ms>=999){
		ms=0;
		budilka_flags|=NEW_SEC_F;
		s_since_Y2K++;
		const time_t tmp=s_since_Y2K;
		rtc_time=gmtime(&tmp);
		//if(rtc_time->tm_sec==00) budilka_flags|=NEW_MIN_F;

	}else{
		ms++;
	}
}