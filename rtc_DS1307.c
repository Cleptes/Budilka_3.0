/*
 * rtc_DS1337.c
 *
 * Created: 22/04/2017 19:53:19
 *  Author: Jan
 */ 

#include "rtc_DS1307.h"


void dec_to_bcd_time(struct tm *dec_time,rtc_time_t *bcd_time){
	bcd_time->sec=((dec_time->tm_sec/10)<<TIME_10_OFFS)|(dec_time->tm_sec%10);
	bcd_time->min=((dec_time->tm_min/10)<<TIME_10_OFFS)|(dec_time->tm_min%10);
	bcd_time->hour=((dec_time->tm_hour/10)<<TIME_10_OFFS)|(dec_time->tm_hour%10);
	bcd_time->day=((dec_time->tm_mday/10)<<TIME_10_OFFS)|(dec_time->tm_mday%10);
	bcd_time->mon=(((dec_time->tm_mon+1)/10)<<TIME_10_OFFS)|((dec_time->tm_mon+1)%10);
	bcd_time->year=(((dec_time->tm_year-100)/10)<<TIME_10_OFFS)|((dec_time->tm_year-100)%10);
	bcd_time->wday=dec_time->tm_wday+1;
}

void bcd_to_dec_time(rtc_time_t *bcd_time,struct tm *dec_time){
	dec_time->tm_sec=(bcd_time->sec>>TIME_10_OFFS)*10+(bcd_time->sec&TIME_10_MASK);
	dec_time->tm_min=(bcd_time->min>>TIME_10_OFFS)*10+(bcd_time->min&TIME_10_MASK);
	dec_time->tm_hour=(bcd_time->hour>>TIME_10_OFFS)*10+(bcd_time->hour&TIME_10_MASK);
	dec_time->tm_mday=(bcd_time->day>>TIME_10_OFFS)*10+(bcd_time->day&TIME_10_MASK);
	dec_time->tm_mon=((bcd_time->mon>>TIME_10_OFFS)*10+(bcd_time->mon&TIME_10_MASK))-1;
	dec_time->tm_year=((bcd_time->year>>TIME_10_OFFS)*10+(bcd_time->year&TIME_10_MASK))+100;
	dec_time->tm_wday=bcd_time->wday-1;	
}

char RTC_init(){
	char err;
	
	TWIStart();
	err=TWIGetStatus();
	if(err!=START_SENT){
		return START_SENT;
	}
	
	err=TWIWrite_err_mode((DS1307_ADDR<<1)|WRITE_BIT);
	if(err!=ADDR_W_AND_ACK){
		return ADDR_W_AND_ACK;
	}
	
	err=TWIWrite_err_mode(RTC_CONTROL_REG);
	if(err!=DATA_AND_ACK){
		return DATA_AND_ACK;
	}
	
	err=TWIWrite_err_mode(0);
	if(err!=DATA_AND_ACK){
		return DATA_AND_ACK;
	}
	
	TWIStop();
	return 0;
}

char RTC_set_time(struct tm *dec_time){
	rtc_time_t *bcd_time;
	dec_to_bcd_time(dec_time,bcd_time);
	char err;
	
	TWIStart();
	err=TWIGetStatus();
	if(err!=START_SENT){
		return START_SENT;
	}
	
	err=TWIWrite_err_mode(TWI_WRITE_MODE);
	if(err!=ADDR_W_AND_ACK){
		return ADDR_W_AND_ACK;
	}
	
	TWIWrite_err_mode(0); // na nulti naslov tm so sekunde
	TWIWrite(bcd_time->sec);
	TWIWrite(bcd_time->min);
	TWIWrite(bcd_time->hour);
	TWIWrite(bcd_time->wday);
	TWIWrite(bcd_time->day);
	TWIWrite(bcd_time->mon);
	TWIWrite(bcd_time->year);
	
	TWIStop();
	return 0;
	
	
	
	return 0;
}

char RTC_get_time(struct tm *dec_time){
	rtc_time_t *bcd_time;
	
	TWIStart();
	
	TWIWrite(TWI_WRITE_MODE);
	TWIWrite(0);
	TWIStart();
	TWIWrite(TWI_READ_MODE);
	bcd_time->sec=TWIReadACK();
	bcd_time->min=TWIReadACK();
	bcd_time->hour=TWIReadACK();
	bcd_time->wday=TWIReadACK();
	bcd_time->day=TWIReadACK();
	bcd_time->mon=TWIReadACK();
	bcd_time->year=TWIReadNACK();
	TWIStop();
	
	bcd_to_dec_time(bcd_time,dec_time);
	return 0;
	
}

char RTC_read_reg(char reg, char *ret_data){
	char err;
	TWIStart();
	err=TWIGetStatus();
	if(err!=START_SENT){
		*ret_data=err;
		return START_SENT;
	}
	TWIWrite((DS1307_ADDR<<1)|READ_BIT);
	err=TWIGetStatus();
	if(err!=ADDR_R_AND_ACK){
		*ret_data=err;
		return ADDR_R_AND_ACK;
	}
	TWIWrite(reg);
	err=TWIGetStatus();
	if(err!=DATA_AND_NACK){
		*ret_data=err;
		return DATA_AND_NACK;
	}
	TWIStart();
	err=TWIGetStatus();
	if(err!=REPEATED_START){
		*ret_data=err;
		return REPEATED_START;
	}
	TWIWrite((DS1307_ADDR<<1)|WRITE_BIT);
	err=TWIGetStatus();
	if(err!=ADDR_W_AND_ACK){
		*ret_data=err;
		return ADDR_W_AND_ACK;
	}
	*ret_data=TWIReadNACK();
	err=TWIGetStatus();
	if(err!=DATA_REC_NACK){
		*ret_data=err;
		return DATA_REC_NACK;
	}
	TWIStop();
	return 0;
}