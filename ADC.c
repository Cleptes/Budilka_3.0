/*
 * ADC.c
 *
 * Created: 8. 05. 2016 14:03:55
 *  Author: Jan
 */ 

#include "ADC.h"

void Init_ADC(){
	// AREF = AVcc
	ADMUX = (1<<REFS0)|(0<<ADLAR);
	// ADC Enable & prescaler: 128
	ADCSRA = (1<<ADEN)|(1<<ADPS2)|(1<<ADPS1)|(1<<ADPS0);
	DIDR0=0x07; //izklop digitalni vhodn buffer
}

unsigned int ADConvert(char ch){
	ch &= 0x07;  
	ADMUX = (ADMUX & 0xf8)|ch; 
	
	// 1x konverzija
	ADCSRA |= (1<<ADSC);
	
	while(ADCSRA & (1<<ADSC));
	
	return (ADC);
}

