/*
 * Budilka_3.0.c
 *
 * Created: 2. 08. 2016 15:23:14
 * Author : Jan
 */ 
/*
 *
 *  TODO: serial z fprintf, lcd da pi�e le do konca vrstice (ni nujno), rtc, button...
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <avr/interrupt.h>
#include <avr/io.h>



#include "uart.h"
#include "vezava.h"
#include "hd44780.h" 
#include "rc5.h"
#include "pwm.h"
#include "timer0_rtc.h"
#include "flags.h"
#include "i2c.h"
#include "rtc_DS1307.h"

#include <util/delay.h>



uint8_t budilka_flags=0;

int main(void){
	//RELAY_DIR|=(1<<RELAY_PIN_VAL);
	//RELAY_PORT|=(1<<RELAY_PIN_VAL);
	 
	lcd_init();
	uart_init(UART_BAUD_SELECT(9600,F_CPU));
	PWM_init(8);
	timer0_rtc_init();
	TWI_init();
	s_since_Y2K=111111;
	//RC5_Init();
	FILE SERIAL_STREM = FDEV_SETUP_STREAM(UART_putc,NULL,_FDEV_SETUP_WRITE);
	stdout=&LCD_stream;
	sei();
	
	

	uint16_t command;
	uint16_t c;
	
	lcd_goto(0,0);
	time_t tmp;
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE){
		tmp=s_since_Y2K;
	}
	
	//printf("%d",gmtime(&tmp));
	
	char i2c_get=5,err=5;
	err=RTC_init();
	if(err==0) fprintf(&SERIAL_STREM,"inicializiru rtc \n");
	else fprintf(&SERIAL_STREM,"init err: %x\n",err);
	
	
	struct tm* tmp_tm;
	tmp_tm=gmtime(&tmp);
	RTC_set_time(tmp_tm);
	/*err=RTC_read_reg(1,&i2c_get);
	if(err==0) fprintf(&SERIAL_STREM,"dobil: %x\n",i2c_get);
	else fprintf(&SERIAL_STREM,"read err: %x, %x\n",i2c_get,err);*/
	
	
	
	char *txt;
	while (1){	
		
		/*if(RC5_NewCommandReceived(&command)){
			RC5_Reset();
			uint8_t cmdnum=RC5_GetCommandBits(command);
			
			printf("%c",cmdnum);
		}*/
		/*c=uart_getc();
		if( !(c&UART_NO_DATA)){
			
			if((unsigned char)c =='s') PWM_start();
			if((unsigned char)c =='p')PWM_stop();
		}*/
		if(budilka_flags&NEW_SEC_F){
			
				lcd_goto(0,0);
				char buff[100];
				struct tm *tmp;
				/*
				ATOMIC_BLOCK(ATOMIC_RESTORESTATE){
					tmp=rtc_time;
				}*/
				//*txt=0;
				err=RTC_read_reg(0x0, txt);
				if(err==0) fprintf(&SERIAL_STREM,"got: 0x%x\n",txt);
				else fprintf(&SERIAL_STREM,"err: 0x%x\n",err);
				/*RTC_get_time(tmp);
				time2asc(tmp,buff);
				printf("%s",buff);
				*/
				budilka_flags&=~NEW_SEC_F;

		}
		if(budilka_flags&NEW_PAK_F){
			
		}
		_delay_ms(1);


    }
}


ISR(BADISR_vect){
	RELAY_DIR|=(1<<RELAY_PIN_VAL);
	RELAY_PORT|=(1<<RELAY_PIN_VAL);
}
