/*
 * i2c.h
 *
 * Created: 22/04/2017 19:06:57
 *  Author: Jan
 */ 


#ifndef I2C_H_
#define I2C_H_

#include <avr/io.h>

#define START_SENT		 0x08
#define ADDR_W_AND_ACK	 0x18
#define DATA_AND_ACK	 0x28
#define DATA_AND_NACK	 0x58
#define REPEATED_START	 0x10
#define ADDR_R_AND_ACK	 0x40
#define DATA_REC_NACK	 0x30
#define DATA_REC_ACK	 0x30


void TWI_init(void);
void TWIStart(void);
void TWIStop(void);
void TWIWrite(uint8_t u8data);
uint8_t TWIReadACK(void);
uint8_t TWIReadNACK(void);
uint8_t TWIGetStatus(void);
char TWIWrite_err_mode(char data);


#endif /* I2C_H_ */