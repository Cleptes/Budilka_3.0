/*
 * rtc_DS1337.h
 *
 * Created: 22/04/2017 19:53:32
 *  Author: Jan
 */ 


#ifndef RTC_DS1337_H_
#define RTC_DS1337_H_

#include <avr/io.h>
#include "i2c.h"
#include <time.h>

#define DS1307_ADDR		0x68
#define READ_BIT		1
#define WRITE_BIT		0
#define TWI_WRITE_MODE	(DS1307_ADDR<<1)|WRITE_BIT
#define TWI_READ_MODE	(DS1307_ADDR<<1)|READ_BIT

#define RTC_CONTROL_REG 0x07
#define TIME_10_OFFS	4
#define TIME_10_MASK	0b00001111

// BCD struktura!
typedef struct{
	char sec;
	char min;
	char hour;
	char day;
	char mon;
	char year;
	char wday;
} rtc_time_t;



char RTC_init();
char RTC_set_time(struct tm *dec_time);
char RTC_get_time(struct tm *dec_time);
char RTC_read_reg(char reg, char *ret_data);

#endif /* RTC_DS1337_H_ */