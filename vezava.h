/*
 * vezava.h
 *
 * Created: 2. 08. 2016 15:25:13
 *  Author: Jan
 */ 

// UPORABLEN TIMER1
#ifndef VEZAVA_H_
#define VEZAVA_H_

#define F_CPU 16000000UL

#define SPEAKER_PIN_VAL	2
#define SPEAKER_PORT	PORTB
#define SPEAKER_DIR		DDRB

#define RELAY_PIN_VAL	3
#define RELAY_PORT		PORTC
#define RELAY_DIR		DDRC

#define LEFTB_PIN		PIND
#define LEFTB_PIN_VAL	3
#define LEFTB_DIR		DDRD

#define RIGHTB_PIN		PIND
#define RIGHTB_PIN_VAL	2
#define RIGHTB_DIR		DDRD

#define CENTERB_PIN		PINB
#define CENTERB_PIN_VAL	3
#define CENTERB_DIR		DDRB

#define IR_PIN_VAL		1
#define IR_PIN			PINC
#define IR_DIR			DDRC

#define SCL_PORT		PORTC
#define SCL_PIN			PINC
#define SCL_DIR			DDRC
#define SCL_PIN_VAL		5

#define SDA_PORT		PORTC
#define SDA_PIN			PINC
#define SDA_DIR			DDRC
#define SDA_PIN_VAL		4


//DODEJ SPEAKER

#endif /* VEZAVA_H_ */