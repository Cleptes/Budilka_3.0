/*
 * timer0_rtc.h
 *
 * Created: 5. 03. 2017 11:03:55
 *  Author: Jan
 */ 


#ifndef TIMER0_RTC_H_
#define TIMER0_RTC_H_

#include <avr/interrupt.h>
#include <time.h>
#include <util/atomic.h>
#include "flags.h"
#include <stdlib.h>

void timer0_rtc_init();
void time2asc(struct tm* time_date,char *buff);
char month2char(char m);

volatile uint16_t ms;
volatile struct tm *rtc_time;
volatile time_t s_since_Y2K;



#endif /* TIMER0_RTC_H_ */