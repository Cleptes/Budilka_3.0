/*
 * i2c_rtc.c
 *
 * Created: 22/04/2017 19:06:41
 *  Author: Jan
 */ 

#include "i2c.h"

char TWIWrite_err_mode(char data){
	TWIWrite(data);
	return TWIGetStatus();
}

void TWI_init(void){
	//set SCL to 100kHz
	TWSR = 0x00;
	TWBR = 72;
	//enable TWI
	TWCR = (1<<TWEN);
}

void TWIStart(void){
	TWCR = (1<<TWINT)|(1<<TWSTA)|(1<<TWEN);
	while ((TWCR & (1<<TWINT)) == 0);
}

//send stop signal
void TWIStop(void){
	TWCR = (1<<TWINT)|(1<<TWSTO)|(1<<TWEN);
}

void TWIWrite(uint8_t u8data){
	TWDR = u8data;
	TWCR = (1<<TWINT)|(1<<TWEN);
	while ((TWCR & (1<<TWINT)) == 0);
}

uint8_t TWIReadACK(void){
	TWCR = (1<<TWINT)|(1<<TWEN)|(1<<TWEA);
	while ((TWCR & (1<<TWINT)) == 0);
	return TWDR;
}

//read byte with NACK
uint8_t TWIReadNACK(void){
	TWCR = (1<<TWINT)|(1<<TWEN);
	while ((TWCR & (1<<TWINT)) == 0);
	return TWDR;
}

uint8_t TWIGetStatus(void){
	uint8_t status;
	//mask status
	status = TWSR & 0xF8;
	return status;
}