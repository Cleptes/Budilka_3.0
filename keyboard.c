/*
 * keyboard.c
 *
 * Created: 17. 04. 2016 10:16:48
 *  Author: Jan
 */ 

#include "keyboard.h"
#include <avr/io.h>

char lastkey=0;

void ReadKBD(){
	static uint8_t prev=0;
	
	
	uint8_t x = (~PIND & 0x3C)>>2;
	switch(x){
		case 1: x=1; break;
		case 2: x=2; break;
		case 4: x=3; break;
		case 8: x=4; break;
		default: x=0;
	}
	if(prev!=x) {
		lastkey=x;
		prev=x;
	}

}

char GetKey(){
	char x=lastkey;
	lastkey=0;
	return x;
}
