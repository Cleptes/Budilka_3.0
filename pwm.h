/*
 * pwm.h
 *
 * Created: 2. 03. 2017 18:12:46
 *  Author: Jan
 */ 


#ifndef PWM_H_
#define PWM_H_

#include <avr/io.h>
#include "vezava.h"

//prescaler value
#define PRE1	_BV(CS10)
#define PRE8	_BV(CS11)
#define PRE64	_BV(CS11)|_BV(CS10)
#define PRE256	_BV(CS12)
#define PRE1024	_BV(CS12)|_BV(CS10)
#define PRE0	0

void PWM_init(int n);
void PWM_start();
void PWM_stop();

int prescaler;

#endif /* PWM_H_ */