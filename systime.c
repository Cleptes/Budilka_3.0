/*
 * systime.c
 *
 * Created: 16. 05. 2016 21:40:17
 *  Author: Jan
 */ 

#include "systime.h"

volatile uint32_t systick;
volatile uint8_t TIME_STATUS;


void Init_T2(){ //kot pwm ocr2a se nastav pol
	TCCR2A=(1<<COM2A1)|(1<<WGM21)|(1<<WGM20);
	OCR2A=0; 
	TCCR2B=(1<<CS20)|(0<<WGM22);
}


void Init_T0(){ //timr za milis
	OCR0A=71; //(18432000/256)/1000-1
	TCCR0A=(1<<WGM01);
	TCCR0B=4;
	TIMSK0=(1<<OCIE0A);
}


uint32_t GetSysTick(){
	uint32_t tmp;
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE){
		tmp=systick;
	}
	return tmp;
}

char HasOneMillisecondPassed(){
	char x=0;
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE){
		if(TIME_STATUS&(1<<F_1ms))
		{
			TIME_STATUS&=~(1<<F_1ms);
			x=1;
			}else{
			x=0;
		}
		
	}
	return x;
}